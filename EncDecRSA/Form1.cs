﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EncDecRSA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void generate()
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }


        public static string SignData(string message, RSAParameters privateKey)
        {
            ASCIIEncoding byteConverter = new ASCIIEncoding();

            byte[] signedBytes;

            using (var rsa = new RSACryptoServiceProvider())
            {
                // Write the message to a byte array using ASCII as the encoding.
                byte[] originalData = byteConverter.GetBytes(message);

                try
                {
                    // Import the private key used for signing the message
                    rsa.ImportParameters(privateKey);

                    // Sign the data, using SHA512 as the hashing algorithm 
                    signedBytes = rsa.SignData(originalData, CryptoConfig.MapNameToOID("SHA512"));
                }
                catch (CryptographicException e)
                {
                    Console.WriteLine(e.Message);
                    return null;
                }
                finally
                {
                    // Set the keycontainer to be cleared when rsa is garbage collected.
                    rsa.PersistKeyInCsp = false;
                }
            }
            // Convert the byte array back to a string message
            return Convert.ToBase64String(signedBytes);
        }

        public static bool VerifyData(string originalMessage, string signedMessage, RSAParameters publicKey)
        {
            bool success = false;
            using (var rsa = new RSACryptoServiceProvider())
            {
                ASCIIEncoding byteConverter = new ASCIIEncoding();

                byte[] bytesToVerify = byteConverter.GetBytes(originalMessage);
                byte[] signedBytes = Convert.FromBase64String(signedMessage);

                try
                {
                    rsa.ImportParameters(publicKey);

                    success = rsa.VerifyData(bytesToVerify, CryptoConfig.MapNameToOID("SHA512"), signedBytes);
                }
                catch (CryptographicException e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
            return success;
        }

        public static string ExportPrivateKey(RSACryptoServiceProvider csp)
        {
            StringWriter outputStream = new StringWriter();
            if (csp.PublicOnly) throw new ArgumentException("CSP does not contain a private key", "csp");
            var parameters = csp.ExportParameters(true);
            using (var stream = new MemoryStream())
            {
                var writer = new BinaryWriter(stream);
                writer.Write((byte)0x30); // SEQUENCE
                using (var innerStream = new MemoryStream())
                {
                    var innerWriter = new BinaryWriter(innerStream);
                    EncodeIntegerBigEndian(innerWriter, new byte[] { 0x00 }); // Version
                    EncodeIntegerBigEndian(innerWriter, parameters.Modulus);
                    EncodeIntegerBigEndian(innerWriter, parameters.Exponent);
                    EncodeIntegerBigEndian(innerWriter, parameters.D);
                    EncodeIntegerBigEndian(innerWriter, parameters.P);
                    EncodeIntegerBigEndian(innerWriter, parameters.Q);
                    EncodeIntegerBigEndian(innerWriter, parameters.DP);
                    EncodeIntegerBigEndian(innerWriter, parameters.DQ);
                    EncodeIntegerBigEndian(innerWriter, parameters.InverseQ);
                    var length = (int)innerStream.Length;
                    EncodeLength(writer, length);
                    writer.Write(innerStream.GetBuffer(), 0, length);
                }

                var base64 = Convert.ToBase64String(stream.GetBuffer(), 0, (int)stream.Length).ToCharArray();
                // WriteLine terminates with \r\n, we want only \n
                outputStream.Write("-----BEGIN RSA PRIVATE KEY-----\n");
                // Output as Base64 with lines chopped at 64 characters
                for (var i = 0; i < base64.Length; i += 64)
                {
                    outputStream.Write(base64, i, Math.Min(64, base64.Length - i));
                    outputStream.Write("\n");
                }
                outputStream.Write("-----END RSA PRIVATE KEY-----");
            }

            return outputStream.ToString();
        }

        public static string ExportPublicKey(RSACryptoServiceProvider csp)
        {
            StringWriter outputStream = new StringWriter();
            var parameters = csp.ExportParameters(false);
            using (var stream = new MemoryStream())
            {
                var writer = new BinaryWriter(stream);
                writer.Write((byte)0x30); // SEQUENCE
                using (var innerStream = new MemoryStream())
                {
                    var innerWriter = new BinaryWriter(innerStream);
                    innerWriter.Write((byte)0x30); // SEQUENCE
                    EncodeLength(innerWriter, 13);
                    innerWriter.Write((byte)0x06); // OBJECT IDENTIFIER
                    var rsaEncryptionOid = new byte[] { 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x01 };
                    EncodeLength(innerWriter, rsaEncryptionOid.Length);
                    innerWriter.Write(rsaEncryptionOid);
                    innerWriter.Write((byte)0x05); // NULL
                    EncodeLength(innerWriter, 0);
                    innerWriter.Write((byte)0x03); // BIT STRING
                    using (var bitStringStream = new MemoryStream())
                    {
                        var bitStringWriter = new BinaryWriter(bitStringStream);
                        bitStringWriter.Write((byte)0x00); // # of unused bits
                        bitStringWriter.Write((byte)0x30); // SEQUENCE
                        using (var paramsStream = new MemoryStream())
                        {
                            var paramsWriter = new BinaryWriter(paramsStream);
                            EncodeIntegerBigEndian(paramsWriter, parameters.Modulus); // Modulus
                            EncodeIntegerBigEndian(paramsWriter, parameters.Exponent); // Exponent
                            var paramsLength = (int)paramsStream.Length;
                            EncodeLength(bitStringWriter, paramsLength);
                            bitStringWriter.Write(paramsStream.GetBuffer(), 0, paramsLength);
                        }
                        var bitStringLength = (int)bitStringStream.Length;
                        EncodeLength(innerWriter, bitStringLength);
                        innerWriter.Write(bitStringStream.GetBuffer(), 0, bitStringLength);
                    }
                    var length = (int)innerStream.Length;
                    EncodeLength(writer, length);
                    writer.Write(innerStream.GetBuffer(), 0, length);
                }

                var base64 = Convert.ToBase64String(stream.GetBuffer(), 0, (int)stream.Length).ToCharArray();
                // WriteLine terminates with \r\n, we want only \n
                outputStream.Write("-----BEGIN PUBLIC KEY-----\n");
                for (var i = 0; i < base64.Length; i += 64)
                {
                    outputStream.Write(base64, i, Math.Min(64, base64.Length - i));
                    outputStream.Write("\n");
                }
                outputStream.Write("-----END PUBLIC KEY-----");
            }

            return outputStream.ToString();
        }

        private static void EncodeIntegerBigEndian(BinaryWriter stream, byte[] value, bool forceUnsigned = true)
        {
            stream.Write((byte)0x02); // INTEGER
            var prefixZeros = 0;
            for (var i = 0; i < value.Length; i++)
            {
                if (value[i] != 0) break;
                prefixZeros++;
            }
            if (value.Length - prefixZeros == 0)
            {
                EncodeLength(stream, 1);
                stream.Write((byte)0);
            }
            else
            {
                if (forceUnsigned && value[prefixZeros] > 0x7f)
                {
                    // Add a prefix zero to force unsigned if the MSB is 1
                    EncodeLength(stream, value.Length - prefixZeros + 1);
                    stream.Write((byte)0);
                }
                else
                {
                    EncodeLength(stream, value.Length - prefixZeros);
                }
                for (var i = prefixZeros; i < value.Length; i++)
                {
                    stream.Write(value[i]);
                }
            }
        }

        private static void EncodeLength(BinaryWriter stream, int length)
        {
            if (length < 0) throw new ArgumentOutOfRangeException("length", "Length must be non-negative");
            if (length < 0x80)
            {
                // Short form
                stream.Write((byte)length);
            }
            else
            {
                // Long form
                var temp = length;
                var bytesRequired = 0;
                while (temp > 0)
                {
                    temp >>= 8;
                    bytesRequired++;
                }
                stream.Write((byte)(bytesRequired | 0x80));
                for (var i = bytesRequired - 1; i >= 0; i--)
                {
                    stream.Write((byte)(length >> (8 * i) & 0xff));
                }
            }
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            CspParameters cspParams = new CspParameters();

            cspParams.ProviderType = 1; // PROV_RSA_FULL

            //cspParams.ProviderName; // CSP name

            cspParams.Flags = CspProviderFlags.UseArchivableKey;

            cspParams.KeyNumber = (int)KeyNumber.Exchange;

            RSACryptoServiceProvider rsaProvider = new RSACryptoServiceProvider(2048, new CspParameters(1));

            string df = ExportPrivateKey(rsaProvider);

            string fd = ExportPublicKey(rsaProvider);


            RSAParameters RSAPublicKeyInfo = rsaProvider.ExportParameters(false);
            RSAParameters RSAPrivateKeyInfo = rsaProvider.ExportParameters(true);

            rsaProvider.ImportParameters(RSAPrivateKeyInfo);

            // Export public key

            string publicKey1 = rsaProvider.ToXmlString(false);
            //File.WriteAllText(Application.StartupPath + "\\PublicKey.xml", publicKey1);

            // Export private/public key pair

            string privateKey1 = rsaProvider.ToXmlString(true);
            //File.WriteAllText(Application.StartupPath + "\\PrivateKey.xml", privateKey1);
            //////////////////////////////////////////////////////

            // Select target CSP

            cspParams = new CspParameters();

            cspParams.ProviderType = 1; // PROV_RSA_FULL

            //cspParams.ProviderName; // CSP name

            rsaProvider = new RSACryptoServiceProvider(2048, new CspParameters(1));



            // Import public key
            StreamReader publicKeyFile = File.OpenText(Application.StartupPath + "\\PublicKey.xml");

            publicKey1 = publicKeyFile.ReadToEnd();

            rsaProvider.FromXmlString(publicKey1);

            // Encrypt plain text

            byte[] plainBytes = Encoding.Unicode.GetBytes(txtPlainText.Text);

            byte[] encryptedBytes = rsaProvider.Encrypt(plainBytes, false);


            string encrypted = Convert.ToBase64String(encryptedBytes);

            txtResultEnkrip.Text = encrypted;


            /////////////////////////////////////////////////////////////////////////
            ///
            cspParams = new CspParameters();


            //cspParams.ProviderType = 1; // PROV_RSA_FULL

            //cspParams.ProviderName; // CSP name

            //rsaProvider = ImportPrivateKey("-----BEGIN RSA PRIVATE KEY----- MIIEogIBAAKCAQEAoAboenXh1+Gzythv50Jz22lo0wXe59iBMsiSA3vN8T7h/MkNP/ePLJyKzOdQpN5a5P3Qc3Y1BpvBzqdIeeJqAMvY+tpmbg9lCsm/KW2bLFNxZv8adFLg8I7rOEKfbF2Vu5r4ui0X92nvjeM+dOQtbBFnuR5kYSZxknvI0iRtkWeghQMWViEoHVLJQd7MkU5VR+iRx/qP3JqlgaucimHOU0e080UAua4tzlNsZxhNJnnVmtn/KUpsOKCSX50NVsk/LhGV8+C+uLq0DeOyeuYlWkID8AIPxBoq3DsrXYwMpJQTKIhOh5zSEWjgRdCkXaOTLNUa3IrmxbTaZjOUH8+JvQIDAQABAoIBAG8VfImNjPFjvn+fkMZyraPwXxClMZ+0/bJ34kvCxCmj4hGsqqLtwRhRrlQgei2IIIZ23h1YP+TPeFBwdPIOxlC5MGyp8JKT4ysC8szaRe4WGlFJ4l4/LeAvBX8vWCVMLWVmZSQvq0RW6DCRP0i7IAC1cE/jlgCORCZQAQ5GtTxKHvVyr/H3QwJVDnwLSB3+zdxfDfXzMYVP1Q9Z2RpaBP7Y+IdQfxD0eSafp+RL64GE9jqF7/73XxWMk5DpW601LnAYJe8IPk8MLNPAijWUyRz6BZZH0uwYrXEr+hdnGeoYEBbPIyFcsu+Gjo5ETu3H9eYa9Ic8mBhzxx11yF6LXy0CgYEAyrHBUvyCxy2skMnA0EVuTugRV+qvpVxMuHOpIZEFmnkdDDL2OW1bbls4Xk+IurXb4QjmIbOWNpgyiAlG9PXNfMAEQqLzlh93el9AadJZYJFrr7Edg/t/UqAJnRB76GHv/oo7FMAG5OipjEC1OC1aFpA62kkqR9JXgCg2qeLSoF8CgYEAyhyatHwfrQY5tLfUfi1ZBzqOHAR8SWm51mSOoTz4g5i41yygVms2t806HVBhT8WDNeNtr67HajsF9VP5KeeN1Vdr76JZ/6z+nmSuvbifDaW49RTsKJZLQW58y9O+i32tKuFk2tWDl1eucL4MDyRHdndm1SUd2hH17pG3Wh3+m2MCgYA+28k8Vxqncj6l2Ct3a1C+H7HhNeKqwWj5esTkp/i44v5jHhrFzGgiXR+p/7hDTsqVixDzOqJG5AQns9jRdrj3CwVHNQmdcLy4snljV9xn2jEvFxo0J/ZgRUk18Dq84kJvCacw1CxPKOgLmrWYalgZzzixISd/hnekQtRQ8wqmuQKBgEdI/XvJB7cUKghGikkSn4MAMR1Kq7eM9cHbZ78Q7SolFJS4Jbk1SXBcjzHFpvzm0heQpqA/ShB6zugvfQM9q3XixqwXnByknI2rP0KlOlwq5mhyv7S1BcgHOzb88QjsA8bLBrJ0elxHzbvwG/q/NgFfrcJhu/ytNwu1E3FSgPm3AoGAOpVj6dvC53gHNT3YVknjXRS9qNF2OS//++W6IdC6w/Xx5qbkKguxgAtU1nJ6zcDtb1BOpwkMdNexMvwMDwAvU9LlVOcqIFMg7X0CwqMBGhLwvlO9zaNU4W3WSv0FLsuVde0b4r59FsV+30h1M+sOX2RCq1ZsGvEn5gh3FyRn8wo=  -----END RSA PRIVATE KEY-----");
            StreamReader privateKeyFile = File.OpenText(Application.StartupPath + "\\PrivateKey.xml");

            privateKey1 = privateKeyFile.ReadToEnd();

            cspParams = new CspParameters();


            //cspParams.ProviderType = 1; // PROV_RSA_FULL

            //cspParams.ProviderName; // CSP name

            //rsaProvider = ImportPrivateKey("-----BEGIN RSA PRIVATE KEY----- MIIEogIBAAKCAQEAoAboenXh1+Gzythv50Jz22lo0wXe59iBMsiSA3vN8T7h/MkNP/ePLJyKzOdQpN5a5P3Qc3Y1BpvBzqdIeeJqAMvY+tpmbg9lCsm/KW2bLFNxZv8adFLg8I7rOEKfbF2Vu5r4ui0X92nvjeM+dOQtbBFnuR5kYSZxknvI0iRtkWeghQMWViEoHVLJQd7MkU5VR+iRx/qP3JqlgaucimHOU0e080UAua4tzlNsZxhNJnnVmtn/KUpsOKCSX50NVsk/LhGV8+C+uLq0DeOyeuYlWkID8AIPxBoq3DsrXYwMpJQTKIhOh5zSEWjgRdCkXaOTLNUa3IrmxbTaZjOUH8+JvQIDAQABAoIBAG8VfImNjPFjvn+fkMZyraPwXxClMZ+0/bJ34kvCxCmj4hGsqqLtwRhRrlQgei2IIIZ23h1YP+TPeFBwdPIOxlC5MGyp8JKT4ysC8szaRe4WGlFJ4l4/LeAvBX8vWCVMLWVmZSQvq0RW6DCRP0i7IAC1cE/jlgCORCZQAQ5GtTxKHvVyr/H3QwJVDnwLSB3+zdxfDfXzMYVP1Q9Z2RpaBP7Y+IdQfxD0eSafp+RL64GE9jqF7/73XxWMk5DpW601LnAYJe8IPk8MLNPAijWUyRz6BZZH0uwYrXEr+hdnGeoYEBbPIyFcsu+Gjo5ETu3H9eYa9Ic8mBhzxx11yF6LXy0CgYEAyrHBUvyCxy2skMnA0EVuTugRV+qvpVxMuHOpIZEFmnkdDDL2OW1bbls4Xk+IurXb4QjmIbOWNpgyiAlG9PXNfMAEQqLzlh93el9AadJZYJFrr7Edg/t/UqAJnRB76GHv/oo7FMAG5OipjEC1OC1aFpA62kkqR9JXgCg2qeLSoF8CgYEAyhyatHwfrQY5tLfUfi1ZBzqOHAR8SWm51mSOoTz4g5i41yygVms2t806HVBhT8WDNeNtr67HajsF9VP5KeeN1Vdr76JZ/6z+nmSuvbifDaW49RTsKJZLQW58y9O+i32tKuFk2tWDl1eucL4MDyRHdndm1SUd2hH17pG3Wh3+m2MCgYA+28k8Vxqncj6l2Ct3a1C+H7HhNeKqwWj5esTkp/i44v5jHhrFzGgiXR+p/7hDTsqVixDzOqJG5AQns9jRdrj3CwVHNQmdcLy4snljV9xn2jEvFxo0J/ZgRUk18Dq84kJvCacw1CxPKOgLmrWYalgZzzixISd/hnekQtRQ8wqmuQKBgEdI/XvJB7cUKghGikkSn4MAMR1Kq7eM9cHbZ78Q7SolFJS4Jbk1SXBcjzHFpvzm0heQpqA/ShB6zugvfQM9q3XixqwXnByknI2rP0KlOlwq5mhyv7S1BcgHOzb88QjsA8bLBrJ0elxHzbvwG/q/NgFfrcJhu/ytNwu1E3FSgPm3AoGAOpVj6dvC53gHNT3YVknjXRS9qNF2OS//++W6IdC6w/Xx5qbkKguxgAtU1nJ6zcDtb1BOpwkMdNexMvwMDwAvU9LlVOcqIFMg7X0CwqMBGhLwvlO9zaNU4W3WSv0FLsuVde0b4r59FsV+30h1M+sOX2RCq1ZsGvEn5gh3FyRn8wo=  -----END RSA PRIVATE KEY-----");

            //privateKey1 = rsaProvider.ToXmlString(true);

            rsaProvider = new RSACryptoServiceProvider(2048, new CspParameters(1));


            // Import private/public key pair

            rsaProvider.FromXmlString(privateKey1);

            //encrypted = "C1CIjKw8yxdIgrtEmJReKvYkFu7M4s9SXbr+V8RmzFmUuhieOdZw8k3KQ0158jJQe+PbzudhrQD5isOP/hpHEjS6gsxwgKorcbRIxUzZdBi68w2xsQD5T17tstDWh68gyMeURUgIPzwGGRJITbiRZBQdxR+JhDoveT5n2Dy0SFSOALobEb9I9iOGb55SpvBNgMecdlc3j/+rOORiPGD66JOtYPaA8bhXtGL/N0uEQ5qJgQHJL0m34TwfnHgfOIPDrQk0HKea77ssURteMkc0zeK6Z8nXZDc3ZErFBHAzEYHSuTFIF9mIlThJjKtjgflXWGSdTfYunNWW7uoI7YhA1w==";
            // Decrypt text
            plainBytes = null;
            plainBytes = rsaProvider.Decrypt(Convert.FromBase64String(encrypted), false);

            //string SignDataOut = SignData("Anis Fajar", "-----BEGIN RSA PRIVATE KEY----- MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDjBeDYpqEfkN2tj+XH3kqDg6xainHHjQ8FhOxLiWZuhxzpT050VOI9I9jQ3aDnDhmR0ZRvKG40DINX5cruPt64/p+ZAeL2atQ6Szf2mYwqLu7R1kRABuYKq9fjhipdNu2vTuoJcksFkrp23IEfUTfU2wVgVWxa2nKQ8ZTAzpmfCRWuuRcBqieMpR91ARNGjD778yW+n6pWIXLozfMpifFY2fc4sOTdhxh4x3q1QiwaPtoTcm2eWrbXohHfIOMGCO2VO9HI/3QYLg1NoYcyWijTdct9PYfNjKYx02nwyvwhiidC6N6/mYecOUtdbBioUKro5c7vRoPePEMdYDG7hYtlAgMBAAECggEAV/yXQGCUYd+LixESRXxp9L67tfuiTWzmPYVe74e5I21KT0JlbcDYTknuZkT/b42Pa2yG0u4giBuxbAerttq3B4vn3y1UAZ0108IDVpf8Mv0tojYhDY+e6NPhQFVnZek8BY3YNwENAoPzsM2VXcXklaKtWsDA1hWEAZkfxfpMn83yscjYRBNds9BAGLbRZLqr5wQLlhlTa3ezoc/TXOCUZVqnEu13jPcQB3nhdji4oBY +2H/CEzy6kp8wfU9JrXlwMelFOsrkU94yfAn/SjJmDB8YkvD0B+pZHBws8ZgquIhYJqu/GvRfcHe0tICQw/OWE7nXHawrDml0JKZsm7DBoQKBgQDvjY6UAlRhzNn0rL4IbTXrh9rcRtfgwD+49IJg2ktIm5HtkU0TfLNmWzlFDt0DjjFRi1OmW8ZRqEDFj6m3M3fNWDImOLz0n7FEQGfUCpCBEth0uMCt27STbNiiwNDHcip2irv2l+TXnlATRiu0POjLzUQmMG19yIwzR6NewQ6AEwKBgQDynBdNFkcpF/FYpqZwFZuABluq2OPU00pP/cZRmNH1CT6PM49An32IXSc8V1sXXWEoRbH7jwdlRbJhY938CqdnPVh0SoN+mKIiNLBagDjCx0FK+ZuARoWUHYnitGacC6oGMnOkRx3OBJymMFrdzX0OhneKrOowyuX6PsXC6WXlpwKBgQCm8gdAXRNDiUkIPG6/XqTqPSaoxvJh3nlKQ5StPNSkiE69urp1bz1zgvW5i+85MBps0uV8FY9zjwd8528GjubaA7IHcxk0fipC/4OcgbRNT73sMmmNgIUrDPs5CZk2tfij3nG6HAdOwTTbCdchaCVxCt+Ioc1ckKwsfKt9OJwLYwKBgF2b+qWkzeo+McJZ8uhWffxk96OxMh9hM1AwBABh7ckVOw04ALMXlvExuDzhG+WlAyLt2WHN6zWZMd3AEdv5JGSCWAnu4lPLguud1tnklYIaLchHDUfubgYba6OMpfbZzU2807/0LLzcLLE3ASiJ7BHZsnBQjcAr2Za398hgFBALAoGAKdoZZROjc4t5ucQ6LQSunUlJvD/zGnQ/cwe6hJR1y3W+TMzZN2WLv3VcCpUoN47lx8O/yrHcas9RrNiByMkiRGkus1Eei6INryfo+Occ7rQsgnMXitZEJdjW+pFGJJc6ziXChxcjFsQK1WRpGgVef7d6rH3Jkm4apxTgfnltjxo= -----END RSA PRIVATE KEY-----");

            //Generate a public/private key pair.  
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();


            // Import public key
            StreamReader publicKeyFile1 = File.OpenText(Application.StartupPath + "\\PublicKey.xml");
            StreamReader privateKeyFile1 = File.OpenText(Application.StartupPath + "\\PrivateKey.xml");

            publicKey1 = publicKeyFile1.ReadToEnd();
            privateKey1 = privateKeyFile1.ReadToEnd();

            RSA.FromXmlString(publicKey1);
            RSA.FromXmlString(privateKey1);

            //Save the public key information to an RSAParameters structure.
            RSAParameters RSAPublicKeyInfo1 = RSA.ExportParameters(false);
            RSAParameters RSAPrivateKeyInfo1 = RSA.ExportParameters(true);

            string message = txtPlainText.Text;

            string signedMessage = SignData(message, RSAPrivateKeyInfo1);

            bool success = VerifyData(message, signedMessage, RSAPublicKeyInfo1);

            

            string decrypted = Encoding.UTF8.GetString(plainBytes).Replace("\0", "");

            txtDekripPlainteks.Text = decrypted;

            if (success == true)
            {
                txtSignature.Text = "Valid Signature";
            }
            else
            {
                txtSignature.Text = "Invalid Signature";
            }
        }
    }
}
